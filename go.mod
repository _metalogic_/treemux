module bitbucket.org/_metalogic_/treemux

go 1.15

require (
	bitbucket.org/_metalogic_/ident v1.1.1
	bitbucket.org/_metalogic_/log v1.4.1
)
