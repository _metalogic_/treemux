// Package treemux is inspired by Julien Schmidt's httprouter. Like httprouter it uses a patricia tree,
// but the path rules are relaxed so that a single path segment may be a wildcard in one
// path and a static token in another. This combines high performance with improved convenience
// in specifying path patterns.
package treemux

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/_metalogic_/ident"
	"bitbucket.org/_metalogic_/log"
)

// HandlerFunc is called with identity (including associated role information), method,
// path and params mapping parameters to values parsed from wildcards and catch-alls in the path.
// The handler may use any or none of these parameters to make its access decision.
// If access should be permitted an http.StatausOK should be returned. Any other status returned will result in access denied.
type HandlerFunc func(method string, path string, checkParams map[string]string, identity ident.Authorizer) int

// LookupResult contains information about a path lookup, which is returned from Lookup and
// can be passed to ServeLookupResult if the request should be served.
type LookupResult struct {
	// StatusCode informs the caller about the result of the lookup.
	// This will generally be `http.StatusNotFound` or `http.StatusMethodNotAllowed` for an
	// error case. On a normal success, the statusCode will be `http.StatusOK`. A redirect code
	// will also be used in the case
	StatusCode  int
	ParamMap    map[string]string
	handler     HandlerFunc
	leafHandler map[string]HandlerFunc // Only has a value when StatusCode is MethodNotAllowed
}

// Dump returns a text representation of the routing tree.
func (t *TreeMux) Dump() string {
	return t.root.dumpTree("", "")
}

func redirectHandler(newPath string, statusCode int) HandlerFunc {
	return func(method, path string, params map[string]string, identity ident.Authorizer) int {
		log.Debugf("redirecting to %s", newPath)
		return http.StatusMovedPermanently
	}
}

func (t *TreeMux) lookup(method, path string) (result LookupResult, found bool) {
	result.StatusCode = http.StatusNotFound
	path = stripQuery(path)
	unescapedPath := path // r.URL.Path
	pathLen := len(path)

	trailingSlash := (path[pathLen-1] == '/' && pathLen > 1)
	if trailingSlash && t.RedirectTrailingSlash {
		path = path[:pathLen-1]
		unescapedPath = unescapedPath[:len(unescapedPath)-1]
	}

	node, handler, params := t.root.search(method, path[1:])
	if node == nil {
		if t.RedirectCleanPath {
			// Path was not found. Try cleaning it up and search again.
			// TODO Test this
			cleanPath := Clean(unescapedPath)
			node, handler, params = t.root.search(method, cleanPath[1:])
			if node == nil {
				// Still nothing found.
				return result, false
			}

			return LookupResult{StatusCode: http.StatusMovedPermanently, handler: redirectHandler(cleanPath, http.StatusMovedPermanently), leafHandler: nil}, true

		}
		// Not found.
		return result, false
	}

	if handler == nil {
		return LookupResult{StatusCode: http.StatusMethodNotAllowed, leafHandler: node.leafHandler}, false
	}

	if !node.isCatchAll || t.RemoveCatchAllTrailingSlash {
		if trailingSlash != node.addSlash && t.RedirectTrailingSlash {
			var h HandlerFunc
			if node.addSlash {
				// Need to add a slash.
				h = redirectHandler(unescapedPath+"/", http.StatusMovedPermanently)
			} else if path != "/" {
				// We need to remove the slash. This was already done at the
				// beginning of the function.
				h = redirectHandler(unescapedPath, http.StatusMovedPermanently)
			}

			if h != nil {
				return LookupResult{StatusCode: http.StatusMovedPermanently, handler: h}, true
			}
		}
	}

	var paramMap map[string]string
	if len(params) != 0 {
		if len(params) != len(node.leafWildcardNames) {
			// Need better behavior here. Should this be a panic?
			panic(fmt.Sprintf("treemux parameter list length mismatch: %v, %v",
				params, node.leafWildcardNames))
		}

		paramMap = make(map[string]string)
		numParams := len(params)
		for index := 0; index < numParams; index++ {
			paramMap[node.leafWildcardNames[numParams-index-1]] = params[index]
		}
	}

	return LookupResult{StatusCode: http.StatusOK, ParamMap: paramMap, handler: handler, leafHandler: nil}, true
}

// Lookup performs a lookup of method and path returning a Lookup result and a boolean.
// The boolean will be true when a handler was found or the lookup resulted in a redirect
// which will point to a real handler. It is false for requests which would result in
// a `StatusNotFound` or `StatusMethodNotAllowed`.
//
// Regardless of the returned boolean's value, the LookupResult may be passed to ServeLookupResult
// to be served appropriately.
func (t *TreeMux) Lookup(method, path string) (result LookupResult, found bool) {
	if t.SafeAddRoutesWhileRunning {
		// In concurrency safe mode, we acquire a read lock on the mutex for any access.
		// This is optional to avoid potential performance loss in high-usage scenarios.
		t.mutex.RLock()
	}

	result, found = t.lookup(method, path)

	if t.SafeAddRoutesWhileRunning {
		t.mutex.RUnlock()
	}

	return result, found
}

// CheckRequest evaluates a method and path returning an HTTP status code for the request
func (t *TreeMux) CheckRequest(method, path string, identity ident.Authorizer) int {
	if t.SafeAddRoutesWhileRunning {
		// In concurrency safe mode, we acquire a read lock on the mutex for any access.
		// This is optional to avoid potential performance loss in high-usage scenarios.
		t.mutex.RLock()
	}

	result, _ := t.lookup(method, path)

	if t.SafeAddRoutesWhileRunning {
		t.mutex.RUnlock()
	}
	if result.handler == nil {
		if result.StatusCode == http.StatusMethodNotAllowed && result.leafHandler != nil {
			return http.StatusMethodNotAllowed
		}
		return http.StatusNotFound
	}
	return result.handler(method, path, result.ParamMap, identity)
}

func stripQuery(uri string) string {
	i := strings.Index(uri, "?")
	if i > 0 {
		return uri[0:i]
	}
	return uri
}
