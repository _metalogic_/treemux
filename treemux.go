package treemux

import (
	"sync"
)

// TreeMux ...
type TreeMux struct {
	root  *node
	mutex sync.RWMutex

	Group

	// RedirectCleanPath allows the router to try clean the current request path,
	// if no handler is registered for it, using CleanPath from github.com/dimfeld/httppath.
	// This is true by default.
	RedirectCleanPath bool

	// RedirectTrailingSlash enables automatic redirection in case router doesn't find a matching route
	// for the current request path but a handler for the path with or without the trailing
	// slash exists. This is true by default.
	RedirectTrailingSlash bool

	// RemoveCatchAllTrailingSlash removes the trailing slash when a catch-all pattern
	// is matched, if set to true. By default, catch-all paths are never redirected.
	RemoveCatchAllTrailingSlash bool

	// EscapeAddedRoutes controls URI escaping behavior when adding a route to the tree.
	// If set to true, the router will add both the route as originally passed, and
	// a version passed through URL.EscapedPath. This behavior is disabled by default.
	EscapeAddedRoutes bool

	// SafeAddRoutesWhileRunning tells the router to protect all accesses to the tree with an RWMutex. This is only needed
	// if you are going to add routes after the router has already begun serving requests. There is a potential
	// performance penalty at high load.
	SafeAddRoutesWhileRunning bool
}

// New constructor for TreeMux
func New() *TreeMux {
	tm := &TreeMux{
		root:                  &node{path: "/"},
		RedirectTrailingSlash: true,
		RedirectCleanPath:     true,
		EscapeAddedRoutes:     false,
	}
	tm.Group.mux = tm
	return tm
}

// NewGroup creates a new TreeMux at path
func (tm *TreeMux) NewGroup(path string) *Group {
	return tm.Group.NewGroup(path)
}

// GET is convenience method for handling GET requests on a group.
func (tm *TreeMux) GET(path string, handler HandlerFunc) {
	tm.Group.Handle("GET", path, handler)
}

// POST is convenience method for handling POST requests on a context group.
func (tm *TreeMux) POST(path string, handler HandlerFunc) {
	tm.Group.Handle("POST", path, handler)
}

// PUT is convenience method for handling PUT requests on a context group.
func (tm *TreeMux) PUT(path string, handler HandlerFunc) {
	tm.Group.Handle("PUT", path, handler)
}

// DELETE is convenience method for handling DELETE requests on a context group.
func (tm *TreeMux) DELETE(path string, handler HandlerFunc) {
	tm.Group.Handle("DELETE", path, handler)
}

// PATCH is convenience method for handling PATCH requests on a context group.
func (tm *TreeMux) PATCH(path string, handler HandlerFunc) {
	tm.Group.Handle("PATCH", path, handler)
}

// HEAD is convenience method for handling HEAD requests on a context group.
func (tm *TreeMux) HEAD(path string, handler HandlerFunc) {
	tm.Group.Handle("HEAD", path, handler)
}

// OPTIONS is convenience method for handling OPTIONS requests on a context group.
func (tm *TreeMux) OPTIONS(path string, handler HandlerFunc) {
	tm.Group.Handle("OPTIONS", path, handler)
}
