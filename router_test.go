package treemux

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"testing"

	"bitbucket.org/_metalogic_/ident"
)

var bearer = ""

func simpleHandler(method, path string, params map[string]string, identity ident.Authorizer) int {
	// log.Infof("Handling %s %s with params %v", method, path, params)
	return http.StatusOK
}

func testMethods(t *testing.T, useSeparateLookup bool) {
	makeHandler := func(method string) HandlerFunc {
		return func(method, path string, params map[string]string, identity ident.Authorizer) int {
			return http.StatusOK
		}
	}

	router := New()
	router.GET("/user/:param", makeHandler("GET"))
	router.POST("/user/:param", makeHandler("POST"))
	router.PATCH("/user/:param", makeHandler("PATCH"))
	router.PUT("/user/:param", makeHandler("PUT"))
	router.DELETE("/user/:param", makeHandler("DELETE"))

	testMethod := func(method, expect string) {
		result, found := router.Lookup(method, "")

		if result.StatusCode == http.StatusOK && found {
			t.Errorf("Lookup unexpectedly succeeded for method %s", method)
		}
	}

	testMethod("GET", "GET")
	testMethod("POST", "POST")
	testMethod("PATCH", "PATCH")
	testMethod("PUT", "PUT")
	testMethod("DELETE", "DELETE")
	testMethod("HEAD", "")
}
func TestNotFound(t *testing.T) {

	router := New()
	router.GET("/user/abc", simpleHandler)

	result, found := router.Lookup("GET", "/abc/")

	if found {
		t.Error("Expected found = false")
	}

	if result.StatusCode != http.StatusNotFound {
		t.Errorf("Expected status code '%d' but got '%d'", http.StatusNotFound, result.StatusCode)
	}
}

func TestSkipRedirect(t *testing.T) {
	router := New()
	router.RedirectTrailingSlash = false
	router.RedirectCleanPath = false
	router.GET("/slash/", simpleHandler)
	router.GET("/noslash", simpleHandler)

	var status int
	status = router.CheckRequest("GET", "/slash", nil)

	if status != http.StatusNotFound {
		t.Errorf("/slash expected %d, saw %d", http.StatusNotFound, status)
	}

	status = router.CheckRequest("GET", "/noslash/", nil)

	if status != http.StatusNotFound {
		t.Errorf("/noslash expected %d, saw %d", http.StatusNotFound, status)
	}

	status = router.CheckRequest("GET", "//noslash", nil)

	if status != http.StatusNotFound {
		t.Errorf("//noslash expected %d, saw %d", http.StatusNotFound, status)
	}

}

func TestCatchAllTrailingSlashRedirect(t *testing.T) {
	router := New()
	redirectSettings := []bool{false, true}

	router.GET("/abc/*path", simpleHandler)

	testPath := func(path string) {

		var status int
		status = router.CheckRequest("GET", "/abc/"+path, nil)

		endingSlash := strings.HasSuffix(path, "/")

		var expectedCode int
		if endingSlash && router.RedirectTrailingSlash && router.RemoveCatchAllTrailingSlash {
			expectedCode = http.StatusMovedPermanently
		} else {
			expectedCode = http.StatusOK
		}

		if status != expectedCode {
			t.Errorf("Path %s with RedirectTrailingSlash %v, RemoveCatchAllTrailingSlash %v "+
				" expected code %d but saw %d", path,
				router.RedirectTrailingSlash, router.RemoveCatchAllTrailingSlash,
				expectedCode, status)
		}
	}

	for _, redirectSetting := range redirectSettings {
		for _, removeCatchAllSlash := range redirectSettings {
			router.RemoveCatchAllTrailingSlash = removeCatchAllSlash
			router.RedirectTrailingSlash = redirectSetting

			testPath("apples")
			testPath("apples/")
			testPath("apples/bananas")
			testPath("apples/bananas/")
		}
	}

}

func TestWildcardAtSplitNode(t *testing.T) {
	var suppliedParam string
	simpleHandler := func(method, path string, params map[string]string, identity ident.Authorizer) int {
		t.Log(params)
		suppliedParam = params["slug"]
		return http.StatusOK
	}

	router := New()
	router.GET("/pumpkin", simpleHandler)
	router.GET("/passing", simpleHandler)
	router.GET("/:slug", simpleHandler)
	router.GET("/:slug/abc", simpleHandler)

	t.Log(router.root.dumpTree("", " "))

	var status int
	status = router.CheckRequest("GET", "/patch", nil)

	if status != http.StatusOK {
		t.Errorf("Expected HTTP status '%d' for path /patch but got '%d'", http.StatusOK, status)
	}

	if suppliedParam != "patch" {
		t.Errorf("Expected param 'patch', got '%s'", suppliedParam)
	}

	suppliedParam = ""

	status = router.CheckRequest("GET", "/patch/abc", nil)

	if status != http.StatusOK {
		t.Errorf("Expected HTTP status '%d' for path /patch/abc but got '%d'", http.StatusOK, status)
	}

	if suppliedParam != "patch" {
		t.Errorf("Expected param 'patch', got '%s'", suppliedParam)
	}

	status = router.CheckRequest("GET", "/patch/def", nil)

	if status != http.StatusNotFound {
		t.Errorf("Expected HTTP status '%d' for path /patch/abc but got '%d'", http.StatusNotFound, status)
	}

}

func TestSlash(t *testing.T) {
	var param = ""
	handler := func(method, path string, params map[string]string, identity ident.Authorizer) int {
		param = params["param"]

		t.Log("Params: " + params["param"])
		return http.StatusOK
	}

	ymHandler := func(method, path string, params map[string]string, identity ident.Authorizer) int {
		param = params["year"] + " " + params["month"]
		t.Log("YM: " + params["year"] + " " + params["month"])
		return http.StatusOK
	}

	router := New()
	router.GET("/abc/:param", handler)
	router.GET("/year/:year/month/:month", ymHandler)

	var status int
	status = router.CheckRequest("GET", "/abc/de%2ff", nil)

	if status != http.StatusOK {
		t.Error("Expected status OK")
	}

	if param != "de/f" {
		t.Errorf("Expected param de/f, saw %s", param)
	}

	status = router.CheckRequest("GET", "/year/de%2f/month/fg%2f", nil)

	if status != http.StatusOK {
		t.Error("Expected status OK")
	}

	if param != "de/ fg/" {
		t.Errorf("Expected param de/ fg/, saw %s", param)
	}
}

func TestEscapedRoutes(t *testing.T) {
	type testcase struct {
		Route      string
		Path       string
		Param      string
		ParamValue string
	}

	testcases := []*testcase{
		{"/abc/def", "/abc/def", "", ""},
		{"/abc/*star", "/abc/defg", "star", "defg"},
		{"/abc/extrapath/*star", "/abc/extrapath/*lll", "star", "*lll"},
		{"/abc/\\*def", "/abc/*def", "", ""},
		{"/abc/\\\\*def", "/abc/\\*def", "", ""},
		{"/:wild/def", "/*abcd/def", "wild", "*abcd"},
		{"/\\:wild/def", "/:wild/def", "", ""},
		{"/\\\\:wild/def", "/\\:wild/def", "", ""},
		{"/\\*abc/def", "/*abc/def", "", ""},
	}

	escapeCases := []bool{false, true}

	for _, escape := range escapeCases {
		var foundTestCase *testcase
		var foundParamKey string
		var foundParamValue string

		handleTestResponse := func(c *testcase, method, path string, params map[string]string) int {
			foundTestCase = c
			foundParamKey = ""
			foundParamValue = ""
			for key, val := range params {
				foundParamKey = key
				foundParamValue = val
			}
			t.Logf("RequestURI %s found test case %+v", path, c)
			return http.StatusOK
		}

		verify := func(c *testcase) {
			t.Logf("Expecting test case %+v", c)
			if c != foundTestCase {
				t.Errorf("Incorrectly matched test case %+v", foundTestCase)
			}

			if c.Param != foundParamKey {
				t.Errorf("Expected param key %s but saw %s", c.Param, foundParamKey)
			}

			if c.ParamValue != foundParamValue {
				t.Errorf("Expected param key %s but saw %s", c.Param, foundParamKey)
			}
		}

		t.Log("Recreating router")
		router := New()
		router.EscapeAddedRoutes = escape

		for _, c := range testcases {
			t.Logf("Adding route %s", c.Route)
			theCase := c
			router.GET(c.Route, func(method, path string, params map[string]string, identity ident.Authorizer) int {
				return handleTestResponse(theCase, method, path, params)
			})
		}

		for _, c := range testcases {
			escapedPath := (&url.URL{Path: c.Path}).EscapedPath()
			escapedIsSame := (escapedPath == c.Path)

			var status int
			status = router.CheckRequest("GET", c.Path, nil)
			if status != http.StatusOK {
				t.Errorf("Escape %v test case %v got status '%d'", escape, c, status)
			}
			verify(c)

			if !escapedIsSame {
				status = router.CheckRequest("GET", escapedPath, nil)
				if router.EscapeAddedRoutes {
					// Expect a match
					if status != http.StatusOK {
						t.Errorf("Escape %v test case %v got status '%d'", escape, c, status)
					}
					verify(c)
				} else {
					// Expect a non-match if the parameter isn't a wildcard.
					if foundParamKey == "" && status != http.StatusNotFound {
						t.Errorf("Escape %v test case %v expected status '%d', got '%d'", escape, c, http.StatusNotFound, status)
					}
				}
			}
		}
	}
}

// Create a bunch of paths for testing.
func createRoutes(numRoutes int) []string {
	letters := "abcdefghijhklmnopqrstuvwxyz"
	wordMap := map[string]bool{}
	for i := 0; i < numRoutes/2; i++ {
		length := (i % 4) + 4

		wordBytes := make([]byte, length)
		for charIndex := 0; charIndex < length; charIndex++ {
			wordBytes[charIndex] = letters[(i*3+charIndex*4)%len(letters)]
		}
		wordMap[string(wordBytes)] = true
	}

	words := make([]string, 0, len(wordMap))
	for word := range wordMap {
		words = append(words, word)
	}

	routes := make([]string, 0, numRoutes)
	createdRoutes := map[string]bool{}
	rand.Seed(0)
	for len(routes) < numRoutes {
		first := words[rand.Int()%len(words)]
		second := words[rand.Int()%len(words)]
		third := words[rand.Int()%len(words)]
		route := fmt.Sprintf("/%s/%s/%s", first, second, third)

		if createdRoutes[route] {
			continue
		}
		createdRoutes[route] = true
		routes = append(routes, route)
	}

	return routes
}

// TestWriteConcurrency ensures that the router works with multiple goroutines adding
// routes concurrently.
func TestWriteConcurrency(t *testing.T) {
	router := New()

	// First create a bunch of routes
	numRoutes := 10 // 000
	routes := createRoutes(numRoutes)

	wg := sync.WaitGroup{}
	addRoutes := func(base int, method string) {
		for i := 0; i < len(routes); i++ {
			route := routes[(i+base)%len(routes)]
			// t.Logf("Adding %s %s", method, route)
			router.Handle(method, route, simpleHandler)
		}
		wg.Done()
	}

	wg.Add(5)
	go addRoutes(100, "GET")
	go addRoutes(200, "POST")
	go addRoutes(300, "PATCH")
	go addRoutes(400, "PUT")
	go addRoutes(500, "DELETE")
	wg.Wait()

	handleRequests := func(method string) {
		for _, route := range routes {
			// t.Logf("Serving %s %s", method, route)
			var status int

			status = router.CheckRequest(method, route, nil)
			if status != http.StatusOK {
				t.Errorf("%s %s request failed with status code '%d'", method, route, status)
			}
		}
	}

	handleRequests("GET")
	handleRequests("POST")
	handleRequests("PATCH")
	handleRequests("PUT")
	handleRequests("DELETE")
}

// TestReadWriteConcurrency ensures that when SafeAddRoutesWhileRunning is enabled,
// the router is able to add routes while serving traffic.
// TODO fix this test

/* func TestReadWriteConcurrency(t *testing.T) {
	router := New()
	router.SafeAddRoutesWhileRunning = true

	// First create a bunch of routes
	numRoutes := 10000
	routes := createRoutes(numRoutes)

	wg := sync.WaitGroup{}
	addRoutes := func(base int, method string, routes []string) {
		for i := 0; i < len(routes); i++ {
			route := routes[(i+base)%len(routes)]
			// t.Logf("Adding %s %s", method, route)
			router.Handle(method, route, simpleHandler)
		}
		wg.Done()
	}

	handleRequests := func(base int, method string, routes []string, requireFound bool) {
		for i := 0; i < len(routes); i++ {
			route := routes[(i+base)%len(routes)]
			result, found := router.Lookup(method, route)
			if requireFound && !found {
				t.Error("Expected found = true")
			}

			status := CheckRequest(method, route, result, nil)

			if status != http.StatusOK && code != http.StatusMethodNotAllowed {
				t.Errorf("%s %s request failed with status code '%d'", method, route, status)
			}
		}
		wg.Done()
	}

	wg.Add(12)
	initialRoutes := routes[0 : numRoutes/10]
	addRoutes(0, "GET", initialRoutes)
	handleRequests(0, "GET", initialRoutes, true)

	concurrentRoutes := routes[numRoutes/10:]
	go addRoutes(100, "GET", concurrentRoutes)
	go addRoutes(200, "POST", concurrentRoutes)
	go addRoutes(300, "PATCH", concurrentRoutes)
	go addRoutes(400, "PUT", concurrentRoutes)
	go addRoutes(500, "DELETE", concurrentRoutes)
	go handleRequests(50, "GET", routes, false)
	go handleRequests(150, "POST", routes, false)
	go handleRequests(250, "PATCH", routes, false)
	go handleRequests(350, "PUT", routes, false)
	go handleRequests(450, "DELETE", routes, false)

	wg.Wait()

	// Finally check all the routes and make sure they exist.
	wg.Add(5)
	handleRequests(0, "GET", routes, true)
	handleRequests(0, "POST", concurrentRoutes, true)
	handleRequests(0, "PATCH", concurrentRoutes, true)
	handleRequests(0, "PUT", concurrentRoutes, true)
	handleRequests(0, "DELETE", concurrentRoutes, true)
}
*/
