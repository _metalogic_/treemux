package treemux

import (
	"fmt"
	"net/http"
	"testing"

	"bitbucket.org/_metalogic_/ident"
)

func echo(method, path string, params map[string]string) string {
	return fmt.Sprintf("%s: %s, %v", method, path, params)
}

func TestSubGroupSlashMapping(t *testing.T) {
	r := New()
	g := r.NewGroup("/foo")

	g.GET("/", func(method, path string, params map[string]string, identity ident.Authorizer) int {
		t.Log(echo(method, path, params))
		return http.StatusOK
	})

	result, found := r.Lookup("GET", "/foo")

	if !found {
		t.Error("GET /foo not found")
	}

	if result.StatusCode != http.StatusMovedPermanently { //should get redirected
		t.Error(`/foo on NewGroup("/foo").GET("/") should result in redirect response`)
	}

	result, found = r.Lookup("GET", "/foo/")

	if result.StatusCode != http.StatusOK {
		t.Error(`/foo/ on NewGroup("/foo").GET("/) should result in ok response`)
	}
}

func TestSubGroupEmptyMapping(t *testing.T) {
	r := New()
	r.NewGroup("/foo").GET("", func(method, path string, params map[string]string, identity ident.Authorizer) int {
		t.Log(echo(method, path, params))
		return http.StatusOK
	})
	result, found := r.Lookup("GET", "/foo")

	if !found {
		t.Error("did not find path for GET /foo")
	}
	if result.StatusCode != http.StatusMovedPermanently {
		t.Errorf("/foo on NewGroup(\"/foo\").GET(\"\") should result in '%d' got '%d'", http.StatusOK, result.StatusCode)
	}
}

func TestInvalidHandle(t *testing.T) {
	defer func() {
		if err := recover(); err == nil {
			t.Error("Bad handle path should have caused a panic")
		}
	}()
	New().NewGroup("/foo").GET("bar", nil)
}

func TestInvalidSubPath(t *testing.T) {
	defer func() {
		if err := recover(); err == nil {
			t.Error("Bad group sub-path should have caused a panic")
		}
	}()
	New().NewGroup("/foo").NewGroup("bar")
}

func TestInvalidPath(t *testing.T) {
	defer func() {
		if err := recover(); err == nil {
			t.Error("Bad group base path should have caused a panic")
		}
	}()
	New().NewGroup("foo")
}
